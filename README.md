# JHipster Playground
This application was created to test JHipster microservices generator.

## GCP Configurations
You need to create a postgresql instance in GCP SQL for production environment.

After creating postgres instance and before deploying your application, you must to make some configurations into your cluster:
- Creating a proxy user to you SQL instance:<br>
  `gcloud sql users create proxyuser cloudsqlproxy~% --instance=[INSTANCE-NAME] --password=[INSTANCE-PASSWORD]`

Create a credentials.json file with your credentials into your cluster /secret
- `mkdir /secrets`
- `echo [YOUR-JSON-CREDENTIALS] > /secrets/credentials.json`

Create secrets for container proxy to SQL
- `kubectl create secret generic cloudsql-instance-credentials --from-file=credentials.json=/secrets/credentials.json --namespace=prod`

- `kubectl create secret generic cloudsql-db-credentials --from-literal=username=[PROXY_USER] --from-literal=password=[INSTANCE-PASSWORD] --namespace=prod`

## Deploy
You'll need to create a prod namespace
- `kubectl create namespace prod`

You also need to deploy an instance of registry into your kubernetes cluster to run this application.
- https://gitlab.com/elziojr/jhipster-playground-registry

<br><br>
That's it. Enjoy :)