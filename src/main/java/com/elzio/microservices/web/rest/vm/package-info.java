/**
 * View Models used by Spring MVC REST controllers.
 */
package com.elzio.microservices.web.rest.vm;
